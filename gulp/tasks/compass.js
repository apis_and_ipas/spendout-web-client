var compass      = require('gulp-compass'),
    gulp         = require('gulp'),
    replace      = require('gulp-replace'),
    handleErrors = require('../util/handleErrors');

gulp.task('compass', function() {
    return gulp.src('assets/scss/style.scss')
        .pipe(compass({
            config_file: 'config.rb',
            css: 'public',
            sass: 'assets/scss',
            bundle_exec: true
        }))
        .pipe(replace("../assets/images", '/images'))
        .pipe(gulp.dest('public/'))
        .on('error', handleErrors);
});