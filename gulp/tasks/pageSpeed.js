var gulp = require('gulp'),
    pagespeed = require('psi');

// Run PageSpeed Insights
// Update `url` below to the public URL for your site
gulp.task('pagespeed', pagespeed.bind(null, {
  // By default, we use the PageSpeed Insights
  // free (no API key) tier. You can use a Google
  // Developer API key if you have one. See
  // http://goo.gl/RkN0vE for info key: 'YOUR_API_KEY'
  url: 'http://spendout-staticweb.s3-website-us-east-1.amazonaws.com/',
  // strategy: 'mobile'
  strategy: 'desktop'
}));