var gulp = require('gulp'),
    fs = require('fs'),
    s3   = require("gulp-s3");



gulp.task('deploy', function(){
    var config = fs.readFileSync(__dirname + '/../../aws-config.json'),
        aws = JSON.parse(config);

    gulp.src(__dirname + '/../../public/**')
        .pipe(s3(aws));

});