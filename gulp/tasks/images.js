var gulp     = require('gulp'),
    changed  = require('gulp-changed'),
    imagemin = require('gulp-imagemin');

gulp.task('images',  function() {
    var dest = './public/images';

    return gulp.src('./assets/images/raw/**')
        .pipe(changed(dest)) // Ignore unchanged files
        .pipe(imagemin()) // Optimize
        .pipe(gulp.dest(dest));
});

gulp.task('icons',['compass'],  function() {
    var dest = './public/images';

    return gulp.src('./assets/images/icons-*.png')
        .pipe(changed(dest)) // Ignore unchanged files
        .pipe(imagemin()) // Optimize
        .pipe(gulp.dest(dest));
});