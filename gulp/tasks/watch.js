var gulp = require('gulp');

gulp.task('watch', ['setWatch', 'browserSync'], function() {
    gulp.watch('assets/scss/**/*.scss', ['compass']);
    gulp.watch('assets/images/**', ['images']);
    gulp.watch('assets/fonts/**', ['fonts']);
});