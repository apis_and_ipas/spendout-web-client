# SpendOut Webclient

```
    app/                    Home of the Angular Application
    assets/                 Preprocessed Assets
        fonts/              Fonts - copied as-is to /public/fonts
        images/             
            raw/            Raw Images - will be optimized and copied to public/images
            icons/          Icons will be compiled into a sprite sheet
            icons-2x/       Corelating @2x sprite sheet.
        scss/               SCSS...
    gulp/                   
        tasks/              All Gulp tasks
        util/               Utilities for Gulp Tasks
    public/                 the compiled/optimized app and our index.html
    config.rb               Compass configuration
    gulpfile.js             Gulp point of entry, simply requires gulp/index.js
    package.json            Package metainfo
    README.md               This file.
```

## Philosophy
Spendout web client was built a [Static Web App](http://www.staticapps.org/) and attempts to subscribe to methods described there.

## Local installation
1. Go to [NodeJS.org](http://nodejs.org/) and download the installer for your platform `node -v` and `npm -v`. 
* `git clone` this repository and `cd` in to the project.
* Run `npm install` to resolve npm dependencies.  All other dependencies via Bower(front-end) and Bundler(Ruby) will be install automattically after the NPM dependency installation completes
* Run `gulp`.  This will runn all the scripts and start up a BrowserSync-enabled development server on port 3000

## About the Stack
* Browserify is used to build our app out of tiny CommonJS modules
* AngularJS is the clientside framework with help from several extension libraries
    * [NGCookies](https://docs.angularjs.org/api/ngCookies/service/$cookieStore)
    * [NGAnimate](http://www.nganimate.org/)
    * [Angular-UI-Router](https://github.com/angular-ui/ui-router/wiki)
    * [Angular-UI-Bootstrap](http://angular-ui.github.io/bootstrap/)
* Stylesheets are built upon [Bootstrap](http://getbootstrap.com/)'s [Official SCSS port](https://github.com/twbs/bootstrap-sass) and the [Compass utility library](http://compass-style.org/). In places, 
the [new SCSS BEM style syntax is used](http://mikefowler.me/2013/10/17/support-for-bem-modules-sass-3.3/).

## Resources
[scotch.io](http://scotch.io/) - good Angular tutorial/video resource  
[egghead.io](http://egghead.io/) - good Angular tutorial/video resource  
[Angular w/ Browserify](https://www.youtube.com/watch?v=NTPutZ99XWY) - talk from ngConf about pairing Angular w Browserify  
[Gulp + Browserify: The Everything Post](http://viget.com/extend/gulp-browserify-starter-faq) - pretty comprehensive article on using gulp & browserify
[Requiring vs Browserifying Angular](http://developer.telerik.com/featured/requiring-vs-browerifying-angular/) - article of interest
[Best Practices for Building Angular.js Apps](https://medium.com/@dickeyxxx/best-practices-for-building-angular-js-apps-266c1a4a6917) - response to above article of interest

## Cool Stuff
* `gulp deploy` - will copy the contents of /public to an s3 bucket as defined in aws-config.json (sample provided)
* `gulp pagespeed` - will run Google's Pagespeed Insights tool against the S3 bucket deployment
