'use strict';
require('angular-ui-router/release/angular-ui-router.min');


// Prevent 300ms delay for touch events
require('fastclick')(document.body);

//Require the main app
require('./app');

// Code for the offcanvas menu toggler .. 
// TODO: make into a directive
$(function(){
    $('[data-toggle="offcanvas"]').click(function offCanvasToggler() {
        $('.row-offcanvas').toggleClass('active')
    });
});