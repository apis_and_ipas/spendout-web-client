'use strict';
require('ui-bootstrap');
require('ui-bootstrap-tpl');
require('ngAnimate');
require('ngCookies');

var fs = require('fs');

// Deps
var 
    common        = require('./modules/common'),
    site          = require('./modules/site'),
    dashboard     = require('./modules/dashboard'),
    expenses      = require('./modules/expenses'),
    budget        = require('./modules/budget'),
    reports       = require('./modules/reports'),
    people        = require('./modules/people'),
    reminders     = require('./modules/reminders'),
    settings      = require('./modules/settings'),
    notifications = require('./modules/notifications')
;

// Module
var 
    AppController = require('./modules/app/application.controller'),
    AppRouter     = require('./modules/app/application.router'),
    AppConfig     = require('./modules/app/application.config')
;

angular.module('app', [
    'ui.router',
    'ui.bootstrap',
    'ngAnimate',
    common.name,
    site.name,
    dashboard.name,
    expenses.name,
    budget.name,
    reports.name,
    people.name,
    reminders.name,
    notifications.name,
    settings.name
])
.config(AppRouter)
.controller('AppController', AppController)
.constant('COOKIE_NAME',     AppConfig.COOKIE_NAME)
.constant('MODE',            AppConfig.MODE)
.constant('API_URL',         AppConfig.API_URL)
.factory('API_ENDPOINT',     AppConfig.API_ENDPOINT)
;









