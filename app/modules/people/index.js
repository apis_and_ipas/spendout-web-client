'use strict';

var PeopleRouter = require('./people.router'),
    PeopleController = require('./people.controller');


module.exports = angular.module('spendOut.people', [])
    .config(PeopleRouter)
    .controller('PeopleController', PeopleController)
;
