'use strict';
var fs = require('fs');

/**
 * @ngInject
 */
function PeopleRouter ($stateProvider) {
    $stateProvider.state('app.people', {
        url: '/people',
        controller: 'PeopleController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8'),
        resolve: {
            // expenses: ['ExpenseService', function(ExpenseService){

            // }]
        }
    });
}

module.exports = PeopleRouter;