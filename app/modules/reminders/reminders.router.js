'use strict';
var fs = require('fs');

/**
 * @ngInject
 */
function RemindersRouter($stateProvider){

    $stateProvider.state('app.reminders', {
        url: '/reminders',
        controller: 'RemindersController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8'),
        resolve: {
            // expenses: ['ExpenseService', function(ExpenseService){

            // }]
        }
    });
    
}

module.exports = RemindersRouter;