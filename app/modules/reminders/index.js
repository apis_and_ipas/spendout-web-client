'use strict';

var RemindersRouter = require('./reminders.router'),
    RemindersController = require('./reminders.controller');


module.exports = angular.module('spendOut.reminders', [])
    .config(RemindersRouter)
    .controller('RemindersController', RemindersController)
;
