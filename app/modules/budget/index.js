'use strict';

var BudgetRouter = require('./budget.router'),
    BudgetController = require('./budget.controller');


module.exports = angular.module('spendOut.budgets', [])
    .config(BudgetRouter)
    .controller('BudgetController', BudgetController)
;
