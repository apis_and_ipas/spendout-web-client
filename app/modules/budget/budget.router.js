'use strict';
var fs = require('fs');

/**
 * @ngInject
 */
function BudgetRouter($stateProvider){
    $stateProvider.state('app.budget', {
        url: '/budget',
        controller: 'BudgetController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8'),
        resolve: {
            // budget: ['BudgetService', function(BudgetService){

            // }]
        }
    });
}
module.exports = BudgetRouter;