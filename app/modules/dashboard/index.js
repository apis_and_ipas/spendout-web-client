'use strict';

var DashboardRouter = require('./dashboard.router'),
    DashboardController = require('./dashboard.controller');


module.exports = angular.module('spendOut.dashboard', [])
    .config(DashboardRouter)
    .controller('DashboardController', DashboardController)
;
