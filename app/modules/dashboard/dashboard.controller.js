'use strict';

/**
 * @ngInject
 */
function DashboardController(API_ENDPOINT, $log) {
    this.name = "DashboardController";
    $log.log(API_ENDPOINT);
}

module.exports = DashboardController;