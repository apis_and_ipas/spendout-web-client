'use strict';
var fs = require('fs');

/* @ngInject */
function DashboardRouter($stateProvider){

    $stateProvider.state('app.dashboard', {
        url: '/dashboard',
        controller: 'DashboardController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8')
    });

}


module.exports = DashboardRouter;