'use strict';

var ReportsRouter = require('./reports.router'),
    ReportsController = require('./reports.controller');


module.exports = angular.module('spendOut.Reportss', [])
    .config(ReportsRouter)
    .controller('ReportsController', ReportsController)
;
