'use strict';
var fs = require('fs');

/**
 * @ngInject
 */
function ExpenseRouter($stateProvider){
    $stateProvider.state('app.reports', {
        url: '/reports',
        controller: 'ReportsController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8'),
        resolve: {
            // reports: ['ReportService', function(ReportService){

            // }]
        }
    });
}

module.exports = ExpenseRouter;