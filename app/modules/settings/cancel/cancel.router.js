'use strict';
var fs = require('fs');

/**
 * @ngInject
 */
function CancelRouter($stateProvider){

    $stateProvider.state('app.settings.cancelAccount', {
        url: '/cancel',
        controller: 'CancelController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8')
    });
        
        
}

module.exports = CancelRouter;