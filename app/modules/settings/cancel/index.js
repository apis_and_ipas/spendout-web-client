'use strict';

var CancelRouter = require('./cancel.router'),
    CancelController = require('./cancel.controller');


module.exports = angular.module('spendOut.settings.cancel', [])
    .config(CancelRouter)
    .controller('CancelController', CancelController)
;
