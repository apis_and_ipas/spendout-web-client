'use strict';

var PasswordRouter = require('./password.router'),
    PasswordController = require('./password.controller');


module.exports = angular.module('spendOut.settings.password', [])
    .config(PasswordRouter)
    .controller('PasswordController', PasswordController)
;
