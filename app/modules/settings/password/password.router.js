'use strict';
var fs = require('fs');


/**
 * @ngInject
 */
function PasswordRouter($stateProvider){

    $stateProvider.state('app.settings.password', {
        url: '/password',
        controller: 'PasswordController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8')
    });

}

module.exports = PasswordRouter;