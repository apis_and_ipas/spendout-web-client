'use strict';
var fs = require('fs');

/**
 * @ngInject
 */
function ProfileRouter($stateProvider){
   
    $stateProvider.state('app.settings.profile', {
        url: '/profile',
        controller: 'ProfileController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8')
    });
}

module.exports = ProfileRouter;