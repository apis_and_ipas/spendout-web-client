'use strict';

var ProfileRouter = require('./profile.router'),
    ProfileController = require('./profile.controller');


module.exports = angular.module('spendOut.settings.profile', [])
    .config(ProfileRouter)
    .controller('ProfileController', ProfileController)
;
