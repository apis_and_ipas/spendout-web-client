'use strict';

var PaymentsRouter = require('./payments.router'),
    PaymentsController = require('./payments.controller');


module.exports = angular.module('spendOut.settings.payments', [])
    .config(PaymentsRouter)
    .controller('PaymentsController', PaymentsController)
;
