'use strict';
var fs = require('fs');


/**
 * @ngInject
 */
function PaymentsRouter($stateProvider){

    $stateProvider.state('app.settings.payments', {
        url: '/payments',
        controller: 'PaymentsController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8')
    });
    
}

module.exports = PaymentsRouter;