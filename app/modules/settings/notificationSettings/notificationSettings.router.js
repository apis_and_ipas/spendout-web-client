'use strict';
var fs = require('fs');

/**
 * @ngInject
 */
function NotificationSettingsRouter($stateProvider){

    $stateProvider.state('app.settings.notifications', {
        url: '/notifications',
        controller: 'NotificationSettingsController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8')
    });
        
        
}

module.exports = NotificationSettingsRouter;