'use strict';

var NotificationSettingsRouter = require('./notificationSettings.router'),
    NotificationSettingsController = require('./notificationSettings.controller');


module.exports = angular.module('spendOut.settings.notificationSettings', [])
    .config(NotificationSettingsRouter)
    .controller('NotificationSettingsController', NotificationSettingsController)
;
