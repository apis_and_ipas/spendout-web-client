'use strict';

var SettingsRouter = require('./settings.router'),
    SettingsController = require('./settings.controller')
;

var profile = require('./profile'),
    billing = require('./billing'),
    password = require('./password'),
    payments = require('./payments'),
    cancel = require('./cancel'),
    notificationSettings = require('./notificationSettings')
;

module.exports = angular.module('spendOut.settings', [
    profile.name,
    billing.name,
    password.name,
    notificationSettings.name,
    payments.name,
    cancel.name
])
.config(SettingsRouter)
.controller('SettingsController', SettingsController)
;
