'use strict';
var fs = require('fs');

/**
 * @ngInject
 */
function SettingsRouter($stateProvider){
   
   $stateProvider.state('app.settings', {
        url: '/settings',
        controller: 'SettingsController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8')
    });
        
}

module.exports = SettingsRouter;