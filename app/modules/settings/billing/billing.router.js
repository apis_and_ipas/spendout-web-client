'use strict';
var fs = require('fs');


/**
 * @ngInject
 */
function BillingRouter($stateProvider){
   
   $stateProvider.state('app.settings.billing', {
        url: '/billing',
        controller: 'BillingController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8'),
        resolve: {
            // cards: ['CreditCardService', function(CCService){
            //     return CCService.getAll();
            // }]
        }
    });
        
        
}

module.exports = BillingRouter;