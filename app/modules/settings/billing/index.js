'use strict';

var BillingRouter = require('./billing.router'),
    BillingController = require('./billing.controller');


module.exports = angular.module('spendOut.settings.billing', [])
    .config(BillingRouter)
    .controller('BillingController', BillingController)
;
