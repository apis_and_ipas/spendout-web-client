'use strict';

/**
 * @ngInject
 */
function SettingsController($scope, $state, $rootScope, $log) {
    $log.info('entered settings!');

    $scope.go = function(route){
        $state.go(route);
    };

    $scope.active = function(route){
        return $state.is(route);
    };

    $scope.$on("$stateChangeSuccess", function() {
        $scope.tabs.forEach(function(tab) {
            tab.active = $scope.active(tab.route);
        });
    });


    $scope.tabs = [
        { heading: 'Profile',          route: 'app.settings.profile',        active: false },
        { heading: 'Password',         route: 'app.settings.password',       active: false },
        { heading: 'Billing',          route: 'app.settings.billing',        active: false },
        { heading: 'Notifications',    route: 'app.settings.notifications',  active: false },
        { heading: 'Payment History',  route: 'app.settings.payments',       active: false },
        { heading: 'Cancel Account',   route: 'app.settings.cancelAccount',  active: false }
    ];

}


module.exports = SettingsController;