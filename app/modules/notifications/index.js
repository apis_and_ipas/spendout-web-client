'use strict';

var NotificationsRouter = require('./notifications.router'),
    NotificationsController = require('./notifications.controller');


module.exports = angular.module('spendOut.notificationss', [])
    .config(NotificationsRouter)
    .controller('NotificationsController', NotificationsController)
;
