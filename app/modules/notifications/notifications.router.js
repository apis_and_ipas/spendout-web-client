'use strict';
var fs = require('fs');

/**
 * @ngInject
 */
function NotificationsRouter($stateProvider){
    $stateProvider.state('app.notifications', {
        url: '/notifications',
        controller: 'NotificationsController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8'),
        resolve: {
            // expenses: ['ExpenseService', function(ExpenseService){

            // }]
        }
    });
}

module.exports = NotificationsRouter;