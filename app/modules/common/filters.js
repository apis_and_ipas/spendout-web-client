'use strict';

/**
 * capitalize
 * @return a function which takes a {string}
 * takes a string and return a capilized version
 */
var capitalize = function capitalizeFilter () {
    return function(input){
        return input.charAt(0).toUpperCase() + input.slice(1);
    };
}

/**
 * titleize
 * @return a function which takes a {string}
 * and return a lowercased version with leading capital
 */
var titleize = function titleizeFilter () {
    return function (input) {
        var words = input.split(' ');
        for (var i = 0; i < words.length; i++) {
              words[i] = words[i].toLowerCase(); // lowercase everything to get rid of weird casing issues
              words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
        }
        return words.join(' ');
    };
};

/**
 * toISOString
 * @return an ISO formatted date string, if passed in 'YYYY-DD-MM HH:mm A' format
*/
var toISOString = function(){
    return function (date){
        return moment(date, 'YYYY-MM-DD HH:mm A').toISOString();
    };
};



/**
 * toUnixTimestamp
 * @return an ISO formatted date string, if passed in 'YYYY-DD-MM HH:mm A' format
 */
var toUnixTimestamp = function(){
    return function (date){
        return moment(date, 'YYYY-MM-DD HH:mm A').unix();
    };
};


/**
 * daterange
 * given an array of expenses and a valid start and end date returns filtered returns
 * cause, you know, it's a filter
 * @return {Array} filtered expenses
 * 
 * TODO: Refactor and replace expense with general collection. Make the date 
 * property configurable, more generic use.
 */
var daterange = function(){
    return function(expenses, _start_date, _end_date){
        var result = [];

        var start_date = (_start_date && !isNaN(Date.parse(_start_date))) ? Date.parse(_start_date) : 0;
        var end_date = (_end_date && !isNaN(Date.parse(_end_date))) ? Date.parse(_end_date) : new Date().getTime();

        if (expenses && expenses.length > 0) {
            $.each(expenses, function (index, expense){
                var expenseDate = new Date(expense.date);

                if (expenseDate >= start_date && expenseDate <= end_date){
                    result.push(expense);
                }
            });

            return result;
        }
    };
};


module.exports = angular.module('spendOut.common.filters', [])
    .filter('capitalize', capitalize)
    .filter('titleize', titleize)
    .filter('toISOString', toISOString)
    .filter('toUnixTimestamp', toUnixTimestamp)
    .filter('daterange', daterange)
;

