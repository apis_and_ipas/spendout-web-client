'use strict';

/* @ngInject */
function LogoutController(AuthService) {
    this.logout = function(){
            AuthService.logout();
    };
}

module.exports = LogoutController;