'use strict';

var 
    AuthService      = require('./auth.service'),
    AuthConfig       = require('./auth.config'),
    LoginController  = require('./login.controller'),
    LogoutController = require('./logout.controller')
;
    
module.exports = angular.module('spendOut.common.auth', ['ngCookies'])
    .run(AuthConfig)
    .factory('AuthService'        , AuthService)
    .controller('LoginController' ,  LoginController)
    .controller('LogoutController',  LogoutController)
;