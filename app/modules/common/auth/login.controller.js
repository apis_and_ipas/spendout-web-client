'use strict';

/* @ngInject */
function LoginController(AuthService) {
    this.credentials = {
        email:  '',
        password: ''
    };

    this.name = "LoginController";

    this.login = function(credentials) {
        AuthService.login(credentials,  true);
    };
}

module.exports = LoginController;