'use strict';

/* @ngInject */
function AuthConfig ($rootScope, $state, $stateParams, $log, $http, $cookieStore, AuthService, COOKIE_NAME, $window) {
    // body...// alertService,
        $window.AuthService  = AuthService;
        $window.$cookieStore = $cookieStore;
        $window.$http        = $http;
        $window.$rootScope   = $rootScope;

        // Updates the cookie when the user changes... bad idea? good idea?
        $rootScope.$watch('user', function(newUser){
            console.log('USER CHANGED ========>');
            console.log(newUser);
            console.log($cookieStore.get(COOKIE_NAME));
            // $cookieStore.put(COOKIE_NAME, newUser);
        });

        // Lockdown all private routes..
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){

            $log.info('[fromState]', fromState);
            $log.info('[toState]', toState);


            if ($cookieStore.get(COOKIE_NAME)){
                var cookie  =  $cookieStore.get(COOKIE_NAME),
                    token   =  cookie.access_token;
                
                AuthService.setToken(token);
            }

            if (!$cookieStore.get(COOKIE_NAME) && toState.data.access !== 'public'){
                $state.go('public.login');
            }

            $rootScope.$on('auth:login:success', function(event, response){
                AuthService.setUpSession(response);
                // alertService.clearAll();
                $state.go('app.dashboard');
            });

            $rootScope.$on('auth:logout:success', function(event, response){
                AuthService.tearDownSession();
                $state.go('public.login');
            });
        });
}

module.exports = AuthConfig;