'use strict';

/* @ngInject */
function AuthService($rootScope, $log, $http, $state, $cookieStore, API_ENDPOINT, COOKIE_NAME){
        function Auth(){
            var auth = this;

            auth.setUpSession = function(response){
                $log.info('inside Auth#setUpSession', response);
                var ck = {
                    access_token: response.data.access_token//,
                    // email: response.data.user.email,
                    // id: response.data.user.id,
                    // phone_number : response.data.user.phone_number || '',
                    // username: response.data.user.username,
                    // status: response.data.user.status,
                    // plan: response.data.user.plan
                };
                $rootScope.user = ck;
                $cookieStore.put(COOKIE_NAME, ck);
                auth.setToken(response.data.access_token);
            };

            auth.setToken = function(token){
                $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            };

            auth.unSetToken = function(){
                delete $http.defaults.headers.common['Authorization'];
            };

            auth.tearDownSession = function(){

                auth.unSetToken();

                $cookieStore.remove(COOKIE_NAME);
            };


            auth.login = function(credentials, forward){
                $log.log(credentials);
                // // TODO: Refactor validation to model throw Error?
                if (!credentials ||
                    !credentials.email ||
                    !credentials.password ) {
                    return;
                }

                // Required params for Spendout web client
                credentials.device_id       = 'WEB';
                credentials.device_name     = 'WEB';
                credentials.device_token    = 'WEB';
                credentials.platform_type   = 'WEB';

                return $http.post(API_ENDPOINT + '/account/authenticate', credentials)
                    .success(function(response){

                        if (response.response_code === 0 && response.response_code_text === 'SUCCESS') {
                            $log.info('auth:login:success', response);

                            // Emit Success event..
                            $rootScope.$broadcast('auth:login:success', response);
                            $state.go('app.dashboard');
                        } else {
                            $log.info('auth:login:error', response);
                            // Emit Error event
                            $rootScope.$broadcast('auth:login:error', response);

                        }


                    })
                    .error(function(response){
                        $log.info('auth:login:error', response);
                        // The API will almost always return a 200 status, so this is probably just formality
                        $rootScope.$broadcast('auth:login:error', response);

                    });
            };

            auth.logout = function(){
                return $http.get(API_ENDPOINT + '/user/token/invalidate')
                    .success(function(response){

                        if (response.response_code === 0 && response.response_code_text === 'SUCCESS') {
                            $log.info('auth:logout:success', response);

                            // Emit Success event..
                            $rootScope.$broadcast('auth:logout:success', response);
                            $state.go('public.login');
                        } else {
                            $log.info('auth:logout:error', response);

                            // Emit Error event
                            $rootScope.$broadcast('auth:logout:error', response);
                        }


                    })
                    .error(function(response){
                        $log.info('auth:logout:error', response);
                        // The API will almost always return a 200 status, so this is probably just formality
                        $rootScope.$broadcast('auth:login:error', response);

                    });
            };


        }
        return new Auth();

}

module.exports = AuthService;