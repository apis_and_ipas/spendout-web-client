'use strict';

/** @ngInject */
function AlertService($rootScope, $timeout){
    var alertService = {};

    $rootScope.alerts = [];

    alertService.add = function(type, msg, timeout) {
        $rootScope.alerts.push({
            type: type,
            msg: msg,
            close: function() {
                return alertService.closeAlert(this);
            }
        }); // timeout

        if (timeout) {
            $timeout(function(){
                alertService.closeAlert(this);
            }, timeout);
        }
    };

    alertService.closeAlertIdx = function(index) {
        return $rootScope.alerts.splice(index, 1);
    };

    alertService.closeAlert = function(alert) {
        return alertService.closeAlertIdx($rootScope.alerts.indexOf(alert));
    };

    alertService.clearAll = function() {
        $rootScope.alerts = [];
        return this;
    };

    // Gross, when and why did I add this?
    // TODO: fix this crap
    $rootScope.closeAlert = alertService.closeAlert;

    return alertService;
}


module.exports = angular.module('spendOut.common.alerts', [])
    .factory('alertService', AlertService)
;