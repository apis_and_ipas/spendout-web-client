'use strict';

/** @ngInject */
function ExpenseService ($http, API_ENDPOINT) {

    return {
        getAllBetween: function(data){
            if (!data ||
                !data.start_date ||
                !data.end_date){
                return;
            }
            return $http.get(API_ENDPOINT + '/expense/aggregated/getAll', data);
        },

        getOverview: function(){
            // Requires a string with a strangely formatte date, YYYY-DD-MM
            // handled in a private function up top.
            var data = {};
            data.date = formattedDate();
            return $http.get(API_ENDPOINT + '/expense/overview', { params: data });
        },

        getAll: function(data){

            var default_data = {
                'skip': 0,
                'take': 10
            };

            data = angular.extend(default_data, data);


            return $http.post(API_ENDPOINT + '/expense/getAll', data);
        },

        getExpensesForPerson: function(data){
            if (!data ||
                !data.skip ||
                !data.take ||
                !data.field) { // Poorly named? Takes a people_id...
                return;
            }
            return $http.post(API_ENDPOINT + '/expense/people/single/getAll', data);
        },

        create: function(data){
            if (!data ||
                !data.description ||
                !data.category_name ||
                !data.category_type ||
                !data.date ||
                !data.amount
                ) {
                throw new Error('Cannot create expense. Invalid date passed', data);
                // return;
            }
            return $http.post(API_ENDPOINT + '/expense/create', data);
        },

        update: function(data){
            if (!data ||
                !data.description ||
                !data.category_name ||
                !data.category_type ||
                !data.date ||
                !data.amount ||
                !data.image) {
                return;
            }
            return $http.post(API_ENDPOINT + '/expense/update', data);
        },

        destroy: function(data){
            return $http.get(API_ENDPOINT + '/expense/delete', { params: data });
        }
    };
    
 
    ///////////// 

    /**
     * Returns today's date formatted as required by the SpendOutAPI
     *
     * @return {[type]} [description]
     */
    var formattedDate = function(){
        var today = new Date(),
        year, month, day;
        year  = '' + today.getFullYear();
        day   = '' +today.getDate();
        month = (today.getMonth() < 9 ? '0' : '') + (today.getMonth() + 1);

        return year + '-' + day + '-' +  month;
    };



}

module.exports = ExpenseService;