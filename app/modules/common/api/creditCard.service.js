'use strict';

/** @ngInject */
function CreditCardService ($http, API_ENDPOINT) {
    return {
        /**
         * [getAll description]
         * @return {[type]} [description]
         */
        getAll: function(){
            return $http.get(API_ENDPOINT + '/user/card/getAll');
        },


        /**
         * [createCard description]
         * @param  {[type]} data [description]
         * @return {[type]}      [description]
         */
        createCard: function(data){
            if (!data ||
                !data.credit_card_number ||
                !data.expiration_month ||
                !data.expiration_year ||
                !data.ccv ||
                !data.name) {

                return;
            }
            return $http.post(API_ENDPOINT + '/user/card/create', data);
        },

        /**
         * [deleteCard description]
         * @param  {[type]} data [description]
         * @return {[type]}      [description]
         */
        deleteCard: function(data){
            if (!data || !data.card_id) {
                return;
            }
            return $http.get(API_ENDPOINT + '/user/card/setDefault', { params: data });
        },


        /**
         * [setDefault description]
         * @param {[type]} data [description]
         */
        setDefault: function(data){
            if (!data || !data.card_id) {
                return;
            }
            return $http.get(API_ENDPOINT + '/user/card/delete', { params: data });
        }

    };
}

module.exports = CreditCardService;