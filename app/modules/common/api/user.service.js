'use strict';

/** @ngInject */
function UserService ($http, API_ENDPOINT) {
    return {
        /**
         * Gets the User object from API
         * @return promise
         */
        getInformation: function(){
            return $http.get(API_ENDPOINT + '/user/getInformation');
        },

        /**
         * Updates user information
         * @param  user profile data  (phone_number, username, email)
         * @return promise
         */
        updateInformation: function(data){
            if (!data ||
                !data.phone_number ||
                !data.username ||
                !data.email ) {
                return;
            }

            return $http.post(API_ENDPOINT + '/user/updateInformation', data);
        },


        /**
         * Updates user password
         * @param  user profile data  (old_password, password)
         * @return promise
         */
        updatePassword: function(data){
            if (!data ||
                !data.old_password ||
                !data.password ) {
                console.log('UserService:updatePassword', !!data);
                console.log('UserService:updatePassword', !!data.old_password);
                console.log('UserService:updatePassword', !!data.password);
                return;
            }
            return $http.post(API_ENDPOINT +  '/user/resetPassword', data);
        },

         /**
         * Fetch all payments
         * @param  pagination params object (skip, take)
         * @return promise
         */
        getAllPayments: function(data){
            if (!data) { return; }

            // set up defaults
            data.skip = data.skip || 0;
            data.take = data.take || 10;

            return $http.post(API_ENDPOINT +  '/user/payment/getAll', data);
        }

    };
}

module.exports = UserService;