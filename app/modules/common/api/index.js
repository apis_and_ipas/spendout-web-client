'use strict';

var 
    UserService       = require('./user.service'),
    CreditCardService = require('./creditCard.service'),
    CategoryService   = require('./category.service'),
    ExpenseService    = require('./expense.service')
;

module.exports = angular.module('spendOut.apiServices', [])
    .factory('UserService'      , UserService)
    .factory('CreditCardService', CreditCardService)
    .factory('CategoryService'  , CategoryService)
    .factory('ExpenseService'   , ExpenseService)
;
