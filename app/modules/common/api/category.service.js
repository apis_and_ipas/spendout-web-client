'use strict';

/** @ngInject */
function CategoryService ($http, API_ENDPOINT) {
    return {
        getAll: function(){
            return $http.get(API_ENDPOINT + '/category/getAll');
        },
        update: function(data){
            if (!data ||
                !data.categories ||
                !(_.isArray(data.categories)) ) {
                return;
            }
            return $http.post(API_ENDPOINT + '/category/update', data);
        }
    };
}

module.exports = CategoryService;