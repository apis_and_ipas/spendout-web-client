'use strict';

/** @ngInject */
function ErrorInterceptorService ($q, $rootScope, ErrorHandlerService) {
    return {
        'response': function responseHandler (response){

            if (response.config.url === 'https://api-preview.spendout.com/v1/account/authenticate' ||
                response.config.url === 'https://api.spendout.com/v1/account/authenticate') {
                console.log('Auth Response!!!!', response.data.data.access_token);
                // AuthX.setToken(response.data.data.access_token);
            }

            //Will only be called for HTTP up to 300
            if (response.data.response_code) {
                // Handle Custom Errors
                switch (Math.abs(response.data.response_code)) {
                    
                    case 1:
                        console.log('SYSTEM_ERROR: ', response.data.system_error);
                        systemErrorHandler(response.data.system_error);
                        break;
                     
                    case 2:
                        console.log('APPLICATION_ERROR: ', response.data.application_error);
                        break;
                    
                    case 0:
                        break;
                    default:
                        break;
                }
            }

            return response;

        },
        'responseError': function responseErrorHandler (argument) {
            return $q.reject(rejection);
        }
    };
}

/** @ngInject */
function ErrorInterceptor ($httpProvider) {
    $httpProvider.interceptors.push(ErrorInterceptorService);
}

module.exports = ErrorInterceptor;