'use strict';

/** @ngInject */
function ErrorHandlerService ($rootScope) {

    return function(error){
        switch (error.error_code_text){
            case 'IN_VALID_ACCESS_TOKEN':
                // Broadcast the IN_VALID_ACCESS_TOKEN event; listener will redirect to login
                $rootScope.$broadcast('system_error:IN_VALID_ACCESS_TOKEN');
                break;
            case 'VALIDATION_ERRORS':
                // Broadcast a serverside validation error event and send the validation messages..
                $rootScope.$broadcast('system_error:VALIDATION_ERRORS', error.validation_fields);
                break;
            default:
                // In the event of an unspecified event, emit and send the whole event
                $rootScope.$broadcast('system_error:unknown', error);
                break;
        }
    };
 
}

module.exports = ErrorHandlerService;