'use strict';

var
    ErrorHandlerConfig  = require('./errorHandler.config'),
    ErrorHandlerService = require('./errorHandler.service'),
    ErrorInterceptor    = require('./error.interceptor')
;

module.exports =  angular.module('spendOut.common.errors', [])
    .run(ErrorHandlerConfig)
    .config(ErrorInterceptor)
    .service('ErrorHandlerService', ErrorHandlerService)
;