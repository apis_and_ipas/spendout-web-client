'use strict';

/** @ngInject */
function ErrorHandlerConfig ($rootScope, $state, alertService) {


    /**
    *   Redirects to login page when a bad access token is used
    */
    $rootScope.$on('system_error:IN_VALID_ACCESS_TOKEN', function(event, data){

        $state.go('public.login');
        alertService.add('error', 'Session has expired, please login!');

    });

    $rootScope.$on('auth:login:error', function(event, data){
        alertService.add('error', 'Wrong email or password!');
    });

    
}

module.exports = ErrorHandlerConfig;