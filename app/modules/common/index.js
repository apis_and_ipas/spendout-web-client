'use strict';

var 
    filters = require('./filters'),
    auth    = require('./auth'),
    api     = require('./api'),
    alerts  = require('./alerts'),
    errors  = require('./errors')
;

module.exports = angular.module('spendOut.common', [
    filters.name,
    auth.name,
    api.name,
    alerts.name,
    errors.name
])
;