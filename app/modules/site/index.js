'use strict';

var SiteRouter = require('./site.router');

module.exports = angular.module('spendOut.site', [])
    .config(SiteRouter);
;
