'use strict';
var fs = require('fs');

/**
 * @ngInject
 */
function SiteRouter($stateProvider){

    $stateProvider
        .state('site', {
            abstract: true,
            template: '<ui-view/>',
            data: {
                access: 'public'
            }
        })
        .state('site.home', {
            url: '/',
            template: fs.readFileSync(__dirname + '/templates/home.html','utf-8')
        })
        .state('site.404', {
            url: '/404',
            template: fs.readFileSync(__dirname + '/templates/404.html','utf-8')
        })
        .state('site.terms', {
            url: '/terms-of-service',
            template: fs.readFileSync(__dirname + '/templates/terms.html','utf-8')
        })
        .state('site.signup', {
            url: '/signup',
            template: fs.readFileSync(__dirname + '/templates/signup.html','utf-8')
        })
        .state('site.login', {
            url: '/login',
            template: fs.readFileSync(__dirname + '/templates/login.html','utf-8'),
            controller: 'LoginController as loginCtrl'
        })
    ;
}

module.exports = SiteRouter;