'use strict';

var ExpenseRouter = require('./expense.router'),
    ExpenseController = require('./expense.controller');


module.exports = angular.module('spendOut.expenses', [])
    .config(ExpenseRouter)
    .controller('ExpenseController', ExpenseController)
;
