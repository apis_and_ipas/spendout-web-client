'use strict';
var fs = require('fs');

/**
 * @ngInject
 */
function ExpenseRouter($stateProvider){
    $stateProvider.state('app.expenses', {
        url: '/expenses',
        controller: 'ExpenseController',
        template: fs.readFileSync(__dirname + '/index.html','utf-8')
    });
}

module.exports = ExpenseRouter;