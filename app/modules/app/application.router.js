'use strict'
var fs = require('fs');

/** @ngInject */
function UserResolve (UserService) {
    return UserService.getInformation();
}

/** @ngInject */
function AppRouter($stateProvider, $urlRouterProvider){

        $stateProvider.state('app', {
            abstract: true,
            template: fs.readFileSync(__dirname + '/index.html','utf-8'),
            data: {
                access: 'restricted'
            },
            resolve: {
                user: UserResolve
            }
        });

        $urlRouterProvider.otherwise('/');
};
module.exports = AppRouter;



            //,
            // resolve: {
            //      // Resolve the currentUser from the session
            //     user: ['UserService', function(UserService){
            //         return UserService.getInformation();
            //     }],
            //     // Resolve the categories list so it's available everywhere.
            //     categories: ['CategoryService', function(CategoryService){
            //         return CategoryService.getAll();
            //     }]
            // },
            // controller: ['$scope', 'user', 'categories',  function($scope, user, categories){
            //     $rootScope.user = user.data.data;
            //     $rootScope.categories = categories.data.data.data;
            //     console.log('cats', categories);
            // }]