'use strict'

/**
 * @ngInject
 */
function AppController($rootScope, $scope){
    this.name = "AppController";
    this.user = $rootScope.user;
};

module.exports = AppController;