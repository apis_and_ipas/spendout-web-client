'use strict'

/* @ngInject */
function APIEndpoint (MODE, API_URL){
    return API_URL[MODE];
}

exports.API_ENDPOINT =  APIEndpoint;
exports.COOKIE_NAME = 'spendout.session';
exports.MODE = 'dev';
exports.API_URL = {
    'dev': 'https://api-preview.spendout.com/v1',
    'production': 'https://api.spendout.com/v1'
};



