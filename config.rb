# Require any additional compass plugins here.
require 'oily_png'
require 'sassy-buttons'
# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "public"
sass_dir = "assets/scss"
fonts_dir = "public/fonts"
images_dir = "assets/images"
sass_options = {:sourcemap => false}
output_style = :compressed #:expanded 